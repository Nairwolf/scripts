#!/usr/bin/env python3
"""
    This script is used to update a personal diary with a specific syntax.
    This is how I'm written my own personal diary :
    # Vendredi 18 Novembre - 25.24374894
    Aujourd'hui, j'ai fait telle chose importante, et j'ai pensé à cette fameuse idée.
    C'était une bonne journée.

    # Jeudi 17 Novembre - 25.23871146
    Journée pluvieuse, comme mon esprit et mon âme....

    This script automatize the writing of the first line. It writes the actual date,
    and display your exact age based on my personal script age_counter.py.
"""

import datetime
from argparse import ArgumentParser

import age_counter

# Locale variables
DAYS = ("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche")
MONTHS = (
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre",
)


def _parse_args():
    parser = ArgumentParser(description="Edit journal and prefix lines with date")

    parser.add_argument(
        "-f", "--file", required=True, dest="filepath", help="Path of file to edit"
    )
    parser.add_argument(
        "-t",
        "--timestamp",
        default="year",
        dest="fmt",
        choices=["year", "day"],
        help="Precision of the timestamp added (default year format)",
    )
    parser.add_argument(
        "-s",
        "--start",
        default="1/1/1970",
        dest="startdate",
        help="Reference date used to compute time spent",
    )
    parser.add_argument(
        "--check-timestamp",
        action="store_true",
        dest="check",
        help="Use this flag if you want to rewrite all timestamp",
    )

    return parser.parse_args()


def get_content(filepath):
    """
        Open the file located at 'filepath' and stores the text in the
        variable 'content' and returns it.
    """
    with open(filepath, "r") as journal:
        content = journal.read()
    return content


def update_text(content, startdate):
    """
        Take the content inside the diary file, and update the first line if
        needed. If the first line doesn't correspond with the actual date,
        this function update the variable 'content' with the actual date at
        the beginning. Moreover, it adds a timestamp which is your precise
        age computed by an other script.
    """
    today = datetime.date.today()
    weekday = DAYS[today.isoweekday() - 1]
    month = MONTHS[today.month - 1]

    date_string = "# {} {} {}".format(weekday, today.day, month)
    date_from_file = content.splitlines()[0].split(" - ")[0]

    if date_string != date_from_file:
        enddate = "{}/{}/{}".format(today.day, today.month, today.year)
        timestamp = str(age_counter.counter(from_date=startdate, to_date=enddate))
        content = date_string + " - " + timestamp + "\n\n" + content

    return content


def check_timestamp(content, startdate):
    """
        Update the content by adding a timestamp if the
        timestamp isn't already present.
    """
    new_lines = []
    for line in content.splitlines():
        new_line = line
        if line and line[0] == "#":
            str_date = line.split(" - ")[0]
            date = _convert_string_date(str_date)
            age = age_counter.counter(from_date=startdate, to_date=date)
            new_line = str_date + " - " + str(age)
        new_lines.append(new_line)
    content = "\n".join(new_lines)
    return content


def write_content(filepath, content):
    """
        Open the file located at 'filepath' and write the string
        variable 'content' inside.
    """
    with open(filepath, "w") as journal:
        journal.write(content)


def _convert_string_date(date):
    """
        date is '# Vendredi 1 Janvier', converts this string into
        '01/01/2016 - 12h00'
    """
    date_list = date.split()
    day = date_list[2]
    month = MONTHS.index(date_list[3]) + 1
    # FIXME: year will always be the current year. This can cause some issues.
    year = datetime.date.today().year

    formated_date = "{}/{}/{}".format(day, month, year)

    return formated_date


if __name__ == "__main__":
    args = _parse_args()

    content = get_content(args.filepath)
    content = update_text(content, args.startdate)
    if args.check:
        content = check_timestamp(content, args.startdate)
    write_content(args.filepath, content)
